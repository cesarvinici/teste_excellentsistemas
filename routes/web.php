<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('', 'ProdutoController@index');
Route::resource('produtos',  'ProdutoController')
                        ->name('index', 'produto.index')
                        ->name('create', "produto.create")
                        ->name('store', 'produto.store')
                        ->name('edit', "produto.edit")
                        ->name('update', 'produto.update')
                        ->name('show', 'produto.show')
                        ->name('destroy', 'produto.destroy'); 



