<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::all();
        return view('produtos/index', array('produtos'=>$produtos));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("produtos/cadastrar");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome'=> 'required',
            'valor' => 'required',
            'descricao' => 'required',
        ]);

        try
        {
            $produto = new Produto();
            $produto->nome = $request->nome;
            $produto->valor = str_replace(',','.',str_replace('.','',$request->valor));
            $produto->descricao = $request->descricao;
            
            if($produto->save())
            {
                if($request->hasFile('imagem'))
                {    
               
                    $imagem = $request->file('imagem');
                    $nomearquivo  = $produto->id.".". $imagem->getClientOriginalExtension();
                    $request->file('imagem')->move(public_path('/images'), $nomearquivo);
                    $produto->imagem = $nomearquivo;  
                    $produto->save();
                    
                }
            }


           
            Session::flash('success', 'Produto Cadastrado com sucesso');
            return redirect()->route('produto.index');
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors("Ocorreu um erro ".$e->getMessage());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $produto)
    {
        return view('produtos.mostrar', ['produto'=>$produto]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function edit(Produto $produto)
    {
        return view('produtos.editar', array('produto' => $produto));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produto $produto)
    {
        
        try
        {
            $produto->nome = $request->nome;
            $produto->valor =  str_replace(',','.',str_replace('.','',$request->valor));
            $produto->descricao = $request->descricao;

            if($request->hasFile('imagem'))
            {    
           
                $imagem = $request->file('imagem');
                $nomearquivo  = $produto->id.".". $imagem->getClientOriginalExtension();
                $request->file('imagem')->move(public_path('/images'), $nomearquivo);
                $produto->imagem = $nomearquivo;   
                
            }

            $produto->save();
            Session::flash('success', 'Produto editado com sucesso');
            return redirect()->route('produto.index');
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors("Ocorreu um erro ".$e->getMessage());
        }
       

        die;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produto $produto)
    {
       @unlink(public_path('/images/'.$produto->imagem));
        $produto->delete();
        return redirect()->route('produto.index');
    }
}
