@extends('layouts.app')
@section('title', 'Criar Produtos')
@section('content')

<div class="page-header mt-3 text-center">
    <h3 class="text-secondary">Editar Produto</h3>
</div>

@if (count($errors) > 0)
<div class="row col-md-6 mx-auto alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-group">
<form action="{{route('produto.update', ['produto' => $produto->id])}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <input type="text" value="{{$produto->nome}}" name="nome" class="col-md-3 offset-md-5 form-control" placeholder="Nome">
    <input type="text" name="valor" value="{{number_format(floatval($produto->valor),2,",",".")}}" class="col-md-3 offset-md-5 form-control mt-2" placeholder="Valor">
        <textarea name="descricao" class="col-md-3 offset-md-5 form-control mt-2" rows="5" placeholder="Descrição">{{$produto->descricao}}</textarea>
        <img class="img col-md-3 offset-md-5" src="">
        <input type="file" name="imagem"  class="col-md-3 offset-md-5 form-control mt-2">
        
    </div>
    <button class="col-md-3 offset-md-5 form-control mt-2 btn btn-primary">Editar</button>
    
</form>
</div>

@endsection