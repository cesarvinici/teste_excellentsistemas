@extends('layouts.app')
@section('content')
<h1>Produto</h1>
<div class="row">
    <div class="col-md-6 col-md-3">
        <ul>
            <li>{{$produto->nome}}</li>
            <li>Preço: {{number_format($produto->valor,2,',','.')}}</li>
            <li>Adicionado em {{date('d/m/Y', strtotime($produto->created_at))}}</li>
        </ul>
        <p>{{$produto->descricao}}</p>
    </div>
    @if(file_exists("./images/".$produto->imagem))
        <div class="col-md-6 col-md-3">
            <a href="{{asset('/images/'.$produto->imagem)}}" class="thumbnail">
                <img src="{{asset('/images/'.$produto->imagem)}}" width="200px" height="200px">
            </a>
        </div>
    @endif
</div>
<form action="{{route('produto.destroy', ['produto' => $produto->id])}}" method="POST">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger">Deletar Produto</button>
</form>

@endsection