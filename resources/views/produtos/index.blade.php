@extends('layouts.app')
@section('content')
@if(Session::has('success'))
<div class="row col-md-6 mx-auto alert alert-success">
    <ul>       
        <li>{{ Session::get('success') }}</li>
    </ul>
</div>
@endif

<div class="row">
    @forelse($produtos as $produto)
    <div class="col-md-4 mt-2">
        <img class="img img-thumbnail" width="200px" width="200px" src="{{asset('/images/'.$produto->imagem)}}"><br>
        <span class="bold">{{$produto->nome}}</span><br>
        <span>R${{number_format(floatval($produto->valor),2,",",".")}}</span><br>
    <a class="btn btn-success mr-1" href="{{route('produto.show', ['produto' => $produto->id])}}">Ver</a><a href="{{route('produto.edit', ['produto' => $produto->id])}}" class="btn btn-danger">Editar</a>
    </div>
    @empty
        <p class="mx-auto text-danger">Não existem produtos na base de dados.</p>
    @endforelse
</div>

@endsection